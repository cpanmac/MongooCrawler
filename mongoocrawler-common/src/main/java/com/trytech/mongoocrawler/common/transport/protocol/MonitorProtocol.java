package com.trytech.mongoocrawler.common.transport.protocol;

import com.alibaba.fastjson.JSONObject;
import com.trytech.mongoocrawler.common.bean.MonitorData;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * Created by coliza on 2018/5/24.
 */
public class MonitorProtocol extends AbstractProtocol {

    private MonitorData monitorData;

    public MonitorProtocol() {
        if (StringUtils.isEmpty(traceId)) {
            this.traceId = UUID.randomUUID().toString();
        }
    }

    public MonitorData getMonitorData() {
        return monitorData;
    }

    public void setMonitorData(MonitorData monitorData) {
        this.monitorData = monitorData;
    }

    @Override
    public ProtocolType getType() {
        return ProtocolType.MONITOR;
    }

    @Override
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", getType().val());
        jsonObject.put("monitorData", monitorData);
        return jsonObject.toJSONString();
    }
}
