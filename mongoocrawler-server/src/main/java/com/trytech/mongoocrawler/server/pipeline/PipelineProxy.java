package com.trytech.mongoocrawler.server.pipeline;

import com.trytech.mongoocrawler.server.CrawlerSession;

import java.net.UnknownHostException;
import java.sql.SQLException;

/**
 * Created by coliza on 2017/6/17.
 */
public class PipelineProxy<T>{
    private CrawlerSession crawlerSession;
    private  AbstractPipeline<T> abstractPipeline;
    public PipelineProxy(AbstractPipeline<T> abstractPipeline){
        this.abstractPipeline = abstractPipeline;
    }
    public void store(T t) throws SQLException, ClassNotFoundException, UnknownHostException {
        abstractPipeline.store(t);
    }

    public CrawlerSession getCrawlerSession() {
        return crawlerSession;
    }

    public void setCrawlerSession(CrawlerSession crawlerSession) {
        this.crawlerSession = crawlerSession;
    }
}
