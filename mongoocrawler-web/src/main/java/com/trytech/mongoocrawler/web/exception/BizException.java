package com.trytech.mongoocrawler.web.exception;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
public class BizException extends Exception {
    private int status;
    private String msg;

    public BizException(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
