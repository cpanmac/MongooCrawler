package com.trytech.mongoocrawler.web.service;

import com.trytech.mongoocrawler.common.bean.MonitorData;
import com.trytech.mongoocrawler.common.transport.protocol.MonitorProtocol;
import org.springframework.stereotype.Service;

/**
 * Created by coliza on 2018/9/16.
 */
@Service
public class ConfigService extends BaseService {

    public MonitorData getConifg() {
        MonitorProtocol monitorProtocol = new MonitorProtocol();
        monitorProtocol = (MonitorProtocol) sendRequest(monitorProtocol);
        return monitorProtocol.getMonitorData();
    }
}
