package com.trytech.mongoocrawler.web.service;

import com.trytech.mongoocrawler.common.enums.CrawlerStatus;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.web.common.RespStatus;
import com.trytech.mongoocrawler.web.exception.BizException;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by coliza on 2018/9/16.
 */
@Service
public class CrawlerService extends BaseService {

    public CrawlerStatus crawl() throws BizException {
        CommandProtocol commandProtocol = new CommandProtocol();
        commandProtocol.setCommand(CommandProtocol.Command.CRAWL);
        commandProtocol.setTraceId(UUID.randomUUID().toString());
        commandProtocol.setData("https://cd.lianjia.com/ershoufang/rs/");
        MonitorProtocol protocol = (MonitorProtocol) sendRequest(commandProtocol);
        if (protocol != null) {
            int status = protocol.getMonitorData().getServerConfig().getRunStatus();
            return CrawlerStatus.from(status);
        }
        throw RespStatus.CRAWLER_SERVICE_ERROR.toException();
    }
}
