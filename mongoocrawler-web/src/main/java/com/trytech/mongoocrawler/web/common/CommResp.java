package com.trytech.mongoocrawler.web.common;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
public class CommResp {
    //状态
    private Status status;

    public static CommResp resp(RespStatus status) {
        CommResp resp = OK();
        resp.setStatus(status.toStatus());
        return resp;
    }

    public static CommResp OK() {
        CommResp resp = new CommResp();
        resp.setStatus(RespStatus.OK.toStatus());
        return resp;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
